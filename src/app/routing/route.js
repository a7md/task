import React, { Component } from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { Layout } from "antd";
import Home from "components/main/home";
import Inbox from "components/main/inbox";
import Admin from "components/main/admin";
import Login from "components/main/login";

const { Content } = Layout;

class Routing extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Router>
            <div>
              <Content style={{ gridRow: 2 }}>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/inbox" component={Inbox} />
                  <Route exact path="/admin" component={Admin} />
                  <Route exact path="/login" component={Login} />
                </Switch>
              </Content>
            </div>
          </Router>
        </Layout>
      </div>
    );
  }
}

export default Routing;
