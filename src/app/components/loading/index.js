import React, { Component } from "react";
import Loader from "react-loader-spinner";

class Loading extends Component {
  render() {
    return (
      <div>
        <div className="loading">
          <Loader type="Rings" color="#295986" height="300" width="400" />
        </div>
      </div>
    );
  }
}
export default Loading;
