import React, { Component } from "react";
import { Icon, Modal } from "antd";
export default class Content extends Component {
  state = { modal: false };
  render() {
    const { modal } = this.state;
    return (
      <div className="table-media">
        <table className="table">
          <thead>
            <tr>
              <th>Created By</th>
              <th>User</th>
              <th>Status</th>
              <th>Type</th>
              <th>Created Date</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>System Admin</td>
              <td>Ahmed</td>
              <td>
                <button className="btn btn-success">Active</button>
              </td>
              <td>Company</td>
              <td>12-5-2018</td>

              <td
                style={{
                  display: "grid",
                  gridTemplateColumns: "1fr 1fr 1fr 1fr"
                }}
              >
                <Icon
                  type="edit"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="stop"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="close"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="bars"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <td>System Admin</td>
              <td>Ahmed</td>
              <td>
                <button className="btn btn-success">Active</button>
              </td>
              <td>Company</td>
              <td>12-5-2018</td>

              <td
                style={{
                  display: "grid",
                  gridTemplateColumns: "1fr 1fr 1fr 1fr"
                }}
              >
                <Icon
                  type="edit"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="stop"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="close"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="bars"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <td>System Admin</td>
              <td>Ahmed</td>
              <td>
                <button className="btn btn-danger">Deactivated</button>
              </td>
              <td>Company</td>
              <td>12-5-2018</td>

              <td
                style={{
                  display: "grid",
                  gridTemplateColumns: "1fr 1fr 1fr 1fr"
                }}
              >
                <Icon
                  type="edit"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="check"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="close"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="bars"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <td>System Admin</td>
              <td>Ahmed</td>
              <td>
                <button className="btn btn-warning">Postopon</button>
              </td>
              <td>Company</td>
              <td>12-5-2018</td>

              <td
                style={{
                  display: "grid",
                  gridTemplateColumns: "1fr 1fr 1fr 1fr"
                }}
              >
                <Icon
                  type="edit"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="check"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="close"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  type="bars"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <Modal
          title="Info"
          visible={modal}
          onOk={() => {
            this.setState({ modal: false });
          }}
          onCancel={() => {
            this.setState({ modal: false });
          }}
        >
          <p>
            It is a long established fact that a reader will be distracted by
            the readable content of a page when looking at its layout. The point
            of using Lorem Ipsum is that it has a more-or-less normal
            distribution of letters, as opposed to using 'Content here, content
            here', making it look like readable English. Many desktop publishing
            packages and web page editors now use Lorem Ipsum as their default
            model text, and a search for 'lorem ipsum' will uncover many web
            sites still in their infancy. Various versions have evolved over the
            years, sometimes by accident, sometimes on purpose (injected humour
            and the like).
          </p>
        </Modal>
      </div>
    );
  }
}
