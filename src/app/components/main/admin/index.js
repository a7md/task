import React, { Component } from "react";
// import { Dropdown, Avatar, Icon } from "antd";
// import { Breadcrumb } from "antd";
import Header from "./header";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import { Link } from "react-router-dom";
const { SubMenu } = Menu;
const { Content, Sider } = Layout;
import MainContent from "./content";

export default class Admin extends Component {
  state = { inbox: true, services: false };

  render() {
    const { inbox, services } = this.state;
    return (
      <div>
        <Layout>
          <Header />
          <Layout>
            <Sider
              style={{
                overflow: "auto",
                height: "100vh",

                left: 0
              }}
              className="header2"
            >
              <Menu mode="inline" defaultSelectedKeys={["1"]}>
                <Menu.Item
                  key="1"
                  onClick={() => {
                    this.setState({
                      inbox: true,
                      services: false
                    });
                  }}
                >
                  <Icon type="user" />
                  <span className="nav-text">Users</span>
                </Menu.Item>
                <Menu.Item
                  key="2"
                  onClick={() => {
                    this.setState({
                      inbox: false,
                      services: true
                    });
                  }}
                >
                  <Icon type="setting" />
                  <span className="nav-text">Roles</span>
                </Menu.Item>
                <Menu.Item key="3">
                  <Icon type="setting" />
                  <span className="nav-text">Groups</span>
                </Menu.Item>
                <Menu.Item key="4">
                  <Icon type="setting" />
                  <span className="nav-text">Authorization</span>
                </Menu.Item>
              </Menu>
            </Sider>
            <Layout style={{ padding: "0 24px 24px" }}>
              <Breadcrumb style={{ margin: "16px 0" }}>
                <Breadcrumb.Item>C panel</Breadcrumb.Item>
                {inbox ? (
                  <Breadcrumb.Item>Users</Breadcrumb.Item>
                ) : (
                  <Breadcrumb.Item>Groups</Breadcrumb.Item>
                )}
              </Breadcrumb>
              <Content
                style={{
                  background: "#fff",
                  padding: 24,
                  margin: 0,
                  minHeight: 280
                }}
              >
                <MainContent inbox={inbox ? "inbox" : "services"} />
              </Content>
            </Layout>
          </Layout>
        </Layout>
        ,
      </div>
    );
  }
}
