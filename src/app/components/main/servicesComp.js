import React, { Component } from "react";
import { Icon, Button, Input, AutoComplete } from "antd";
import Company from "./company";
const { Option } = AutoComplete;
import BreadComp from "./bread";
function onSelect(value) {
  console.log("onSelect", value);
}

function getRandomInt(max, min = 0) {
  return Math.floor(Math.random() * (max - min + 1)) + min; // eslint-disable-line no-mixed-operators
}
function renderOption(item) {
  return (
    <Option key={item.category} text={item.category}>
      <div className="global-search-item">
        <span className="global-search-item-desc"></span>
        <span className="global-search-item-count">{item.count} results</span>
      </div>
    </Option>
  );
}

export default class Services extends Component {
  state = {
    dataSource: [],
    clicked: false
  };
  onSearch = searchText => {
    this.setState({
      dataSource: !searchText
        ? []
        : [searchText, searchText.repeat(2), searchText.repeat(3)]
    });
  };
  onChange = value => {
    this.setState({ value });
  };
  handleSearch = value => {
    this.setState({
      dataSource: value ? value : []
    });
  };

  render() {
    <BreadComp />;
    const { dataSource, clicked } = this.state;
    return !clicked ? (
      <div className="global-search-wrapper">
        <div class="input-group md-form form-sm form-2 pl-0">
          <input
            class="form-control my-0 py-1 lime-border"
            type="text"
            placeholder="Search"
            aria-label="Search"
          />
          <div class="input-group-append">
            <span class="input-group-text lime lighten-2" id="basic-text1">
              <Icon type="search" />
            </span>
          </div>
        </div>
        <div className="services-section">
          <div
            className="hvr-hang"
            onClick={() => {
              this.setState({ clicked: true });
            }}
          >
            <img src="vendor/images/placeholder.png" />
            <div className="title-services">
              {" "}
              <h5>تأهيل شركة</h5>
            </div>
          </div>
          <div
            className="hvr-hang"
            onClick={() => {
              this.setState({ clicked: true });
            }}
          >
            <img src="vendor/images/placeholder.png" />
            <div className="title-services">
              {" "}
              <h5>خدمات اخرى</h5>
            </div>
          </div>
          <div
            className="hvr-hang"
            onClick={() => {
              this.setState({ clicked: true });
            }}
          >
            <img src="vendor/images/placeholder.png" />
            <div className="title-services">
              {" "}
              <h5>خدمات اخرى</h5>
            </div>
          </div>
          <div
            className="hvr-hang"
            onClick={() => {
              this.setState({ clicked: true });
            }}
          >
            <img src="vendor/images/placeholder.png" />

            <div className="title-services">
              {" "}
              <h5>خدمات اخرى </h5>
            </div>
          </div>
          <div
            className="hvr-hang"
            onClick={() => {
              this.setState({ clicked: true });
            }}
          >
            <img src="vendor/images/placeholder.png" />
            <div className="title-services">
              {" "}
              <h5> خدمات اخرى</h5>
            </div>
          </div>
          <div
            className="hvr-hang"
            onClick={() => {
              this.setState({ clicked: true });
            }}
          >
            <img src="vendor/images/placeholder.png" />
            <div className="title-services">
              {" "}
              <h5>خدمات اخرى </h5>
            </div>
          </div>
        </div>
      </div>
    ) : (
      <Company />
    );
  }
}
