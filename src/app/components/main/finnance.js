import React, { Component } from "react";
import {
  Radio,
  Checkbox,
  Row,
  Col,
  Modal,
  Timeline,
  Icon,
  message
} from "antd";
import Loading from "components/loading";

export default class Finnace extends Component {
  state = {
    value: 1,
    modal: false,
    load: false
  };
  onChange = e => {
    console.log("radio checked", e.target.value);
    this.setState({
      value: e.target.value
    });
  };
  onChange2 = checkedValues => {
    console.log("checked = ", checkedValues);
  };
  sendRequest = () => {
    this.setState({ load: true });
    setTimeout(() => {
      this.setState({ load: false });
    }, 1000);
    this.setState({ modal: true });
  };
  render() {
    const { modal, load } = this.state;
    return (
      <>
        {load ? (
          <Loading />
        ) : (
          <div style={{ width: "80vw", margin: "auto", overflow: "auto" }}>
            <form className="table-media">
              <div class="form-group table-media">
                <label htmlfor="inputDisabledEx2">Template Name</label>
                <input type="text" id="inputDisabledEx2" class="form-control" />
                <label htmlfor="inputDisabledEx3">Subject</label>
                <input type="text" id="inputDisabledEx3" class="form-control" />
                <label htmlfor="textarea-char-counter">Message</label>
                <textarea
                  id="textarea-char-counter"
                  class="form-control md-textarea"
                  length="120"
                  rows="3"
                  placeholder="type your text here"
                ></textarea>
                <div
                  style={{
                    display: "grid",
                    gridTemplateColumns: "1fr 1fr",
                    gridGap: "10px"
                  }}
                >
                  <div>
                    <label for="select1">Message Type</label>
                    <select class="browser-default custom-select" id="select1">
                      <option selected>Open this select menu</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div>
                    <label for="select1">Tap Target</label>
                    <select class="browser-default custom-select" id="select1">
                      <option selected>Open this select menu</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                </div>
                <div
                  style={{
                    display: "grid",
                    gridTemplateColumns: "1fr 1fr",
                    gridGap: "10px"
                  }}
                >
                  <div style={{ marginTop: "10px" }}>
                    <label>Send to Group</label>
                    <Checkbox.Group
                      style={{
                        width: "100%",
                        display: "grid",
                        gridGap: "10px"
                      }}
                      onChange={this.onChange2}
                    >
                      <Col>
                        <Checkbox value="A">Top Management</Checkbox>
                      </Col>
                      <Col>
                        <Checkbox value="B">Design Department</Checkbox>
                      </Col>
                      <Col>
                        <Checkbox value="c">Finnacial Department</Checkbox>
                      </Col>
                      <Col>
                        <Checkbox value="d">Supply Department</Checkbox>
                      </Col>
                    </Checkbox.Group>
                  </div>

                  <div
                    style={{
                      display: "grid",
                      gridTemplateRows: "6vh"
                    }}
                  >
                    <label>Set TYPE</label>
                    <Radio.Group
                      onChange={this.onChange}
                      value={this.state.value}
                      style={{ display: "grid" }}
                    >
                      <Radio value={1}>News</Radio>
                      <Radio value={2}>Reports</Radio>
                      <Radio value={3}>Documents</Radio>
                      <Radio value={4}>Media</Radio>
                      <Radio value={4}>Text</Radio>
                    </Radio.Group>
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "grid",
                  justifyContent: "flex-end",
                  gridTemplateColumns: "200px 150px"
                }}
              >
                <button
                  className="btn btn-primary"
                  onClick={() => {
                    message.success("template saved");
                  }}
                >
                  Save Template
                </button>
                <button className="btn ">Cancel</button>
              </div>
              <button
                onClick={this.sendRequest}
                className="btn btn-success "
                style={{ display: "grid", margin: "2% auto" }}
              >
                Send Request
              </button>
            </form>
            <Modal
              title="Timeline"
              visible={modal}
              onOk={() => {
                this.setState({ modal: false });
              }}
              onCancel={() => {
                this.setState({ modal: false });
              }}
            >
              <Timeline mode="alternate">
                <Timeline.Item>Approval</Timeline.Item>
                <Timeline.Item>2015-09-01</Timeline.Item>
                <Timeline.Item
                  color="yellow"
                  dot={
                    <Icon type="clock-circle-o" style={{ fontSize: "16px" }} />
                  }
                >
                  Under Proccess
                </Timeline.Item>
                <Timeline.Item>2015-09-01</Timeline.Item>
                <Timeline.Item
                  color="green"
                  dot={<Icon type="check" style={{ fontSize: "16px" }} />}
                >
                  forward to process
                </Timeline.Item>
                <Timeline.Item>2015-09-01</Timeline.Item>
                <Timeline.Item
                  color="red"
                  dot={<Icon type="close" style={{ fontSize: "16px" }} />}
                >
                  back To Company for updates
                </Timeline.Item>
                <Timeline.Item>2015-09-01</Timeline.Item>
                <Timeline.Item
                  color="blue"
                  dot={<Icon type="setting" style={{ fontSize: "16px" }} />}
                >
                  Under Process
                </Timeline.Item>
                <Timeline.Item>2015-09-01</Timeline.Item>
              </Timeline>
              ,
            </Modal>
          </div>
        )}
      </>
    );
  }
}
