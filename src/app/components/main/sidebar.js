import React, { Component } from "react";

export default class Sidebar extends Component {
  render() {
    return (
      <>
        <Sider
          width={200}
          style={{ background: "#fff" }}
          className="sidebar-cust"
        >
          <Menu
            mode="inline"
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            style={{ height: "100%", borderRight: 0 }}
          >
            <SubMenu
              key="sub1"
              title={
                <span>
                  <Icon type="inbox" />
                  Inbox
                </span>
              }
            ></SubMenu>
            <SubMenu
              key="sub2"
              title={
                <span>
                  <Icon type="setting" />
                  Services
                </span>
              }
            ></SubMenu>
          </Menu>
        </Sider>
      </>
    );
  }
}
