import React, { Component } from "react";
import Statistics from "./stat";
import TableStat from "./table";
import Services from "./servicesComp";
import Company from "./company";
export default class MainContent extends Component {
  render() {
    const { inbox, services } = this.props;

    return inbox === "inbox" ? (
      <>
        <div className="table-media">
          <Statistics />
          <TableStat />
        </div>
      </>
    ) : (
      <Services />
    );
  }
}
