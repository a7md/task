import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Icon, message, Modal } from "antd";
export default class TableStat extends Component {
  state = { modal: false };
  render() {
    const { modal } = this.state;
    return (
      <>
        <table className="table ">
          <thead>
            <tr>
              <th>#</th>
              <th>Type</th>
              <th>Date</th>
              <th>Status</th>
              <th>Action</th>
              <th>Info</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>442</th>
              <td>تأهيل شركه</td>
              <td>12-5-2018</td>
              <td>
                <button className="btn btn-warning">Expired</button>
              </td>
              <td>
                <a
                  href="#"
                  onClick={() => {
                    message.info("Cancel Successfully");
                  }}
                >
                  Cancel
                </a>
              </td>
              <td>
                <Icon
                  type="info-circle"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <th>443</th>
              <td> خدمه أخرى</td>
              <td>12-5-2018</td>
              <td>
                <button className="btn btn-primary">Draft</button>
              </td>
              <td>
                <a
                  href="#"
                  onClick={() => {
                    message.info("Deleted Successfully");
                  }}
                >
                  Delete
                </a>
              </td>
              <td>
                <Icon
                  type="info-circle"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <th>443</th>
              <td> خدمه أخرى</td>
              <td>12-5-2018</td>
              <td>
                <button className="btn btn-primary">Draft</button>
              </td>
              <td>
                <a
                  href="#"
                  onClick={() => {
                    message.info("Deleted Successfully");
                  }}
                >
                  Delete
                </a>
              </td>
              <td>
                <Icon
                  type="info-circle"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <th>445</th>
              <td>تأهيل شركه</td>
              <td>12-5-2018</td>
              <td>
                <button className="btn btn-danger">Rejected</button>
              </td>
              <td>
                <a
                  href="#"
                  onClick={() => {
                    message.info("Reviewed Successfully");
                  }}
                >
                  Review
                </a>
              </td>
              <td>
                <Icon
                  type="info-circle"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <th>445</th>
              <td>تأهيل شركه</td>
              <td>12-5-2018</td>
              <td>
                <button className="btn btn-danger">Rejected</button>
              </td>
              <td>
                <a href="#">Review</a>
              </td>
              <td>
                <Icon
                  type="info-circle"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
            <tr>
              <th>445</th>
              <td>تأهيل شركه</td>
              <td>12-5-2018</td>
              <td>
                <button className="btn btn-success">Under Process</button>
              </td>
              <td>
                <a href="#">Review</a>
              </td>
              <td>
                <Icon
                  type="info-circle"
                  onClick={() => {
                    this.setState({ modal: true });
                  }}
                  style={{ cursor: "pointer" }}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <Modal
          title="Info"
          visible={modal}
          onOk={() => {
            this.setState({ modal: false });
          }}
          onCancel={() => {
            this.setState({ modal: false });
          }}
        >
          <p>
            It is a long established fact that a reader will be distracted by
            the readable content of a page when looking at its layout. The point
            of using Lorem Ipsum is that it has a more-or-less normal
            distribution of letters, as opposed to using 'Content here, content
            here', making it look like readable English. Many desktop publishing
            packages and web page editors now use Lorem Ipsum as their default
            model text, and a search for 'lorem ipsum' will uncover many web
            sites still in their infancy. Various versions have evolved over the
            years, sometimes by accident, sometimes on purpose (injected humour
            and the like).
          </p>
        </Modal>
      </>
    );
  }
}
