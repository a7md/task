import React, { Component } from "react";
// import { Dropdown, Avatar, Icon } from "antd";
import Bread from "./bread";
import Header from "./header";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import { Link } from "react-router-dom";
const { SubMenu } = Menu;
const { Content, Sider } = Layout;
import MainContent from "./content";

export default class Home extends Component {
  state = { inbox: true, services: false };
  inbox() {
    debugger;
    this.setState({ inbox: true, services: false });
  }
  services() {
    this.setState({ inbox: false, services: true });
  }
  render() {
    const { inbox, services } = this.state;
    return (
      <div>
        <Layout>
          <Header />
          <Layout>
            <Sider
              style={{
                overflow: "auto",
                height: "100vh",

                left: 0
              }}
            >
              <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
                <Menu.Item
                  key="1"
                  onClick={() => {
                    this.setState({
                      inbox: true,
                      services: false
                    });
                  }}
                >
                  <Icon type="inbox" />
                  <span className="nav-text">Inbox</span>
                </Menu.Item>
                <Menu.Item
                  key="2"
                  onClick={() => {
                    this.setState({
                      inbox: false,
                      services: true
                    });
                  }}
                >
                  <Icon type="setting" />
                  <span className="nav-text">Services</span>
                </Menu.Item>
              </Menu>
            </Sider>
            <Layout style={{ padding: "0 24px 24px" }}>
              {inbox ? (
                <Bread data={["Home", "Inbox"]} />
              ) : (
                <Bread data={["Home", "Services"]} />
              )}
              <Content
                style={{
                  background: "#fff",
                  padding: 24,
                  margin: 0,
                  minHeight: 280
                }}
              >
                <MainContent inbox={inbox ? "inbox" : "services"} />
              </Content>
            </Layout>
          </Layout>
        </Layout>
        ,
      </div>
    );
  }
}
