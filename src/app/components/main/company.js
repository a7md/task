import React, { Component } from "react";
import { Tabs, Icon } from "antd";
import Finnance from "./finnance";
const { TabPane } = Tabs;
import Bread from "./bread";

export default class Company extends Component {
  render() {
    return (
      <div className="table-media">
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={
              <span>
                <Icon type="money-collect" />
                Finnance
              </span>
            }
            key="1"
          >
            <Finnance />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="dashboard" />
                Administrative
              </span>
            }
            key="2"
          >
            Administrative
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="user" />
                Tecnical
              </span>
            }
            key="3"
          >
            Tecnical
          </TabPane>
        </Tabs>
      </div>
    );
  }
}
