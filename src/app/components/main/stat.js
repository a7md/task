import React, { Component } from "react";

import { Pie } from "react-chartjs-2";
class Statistics extends Component {
  state = { data: [1, 1, 2, 2] };

  render() {
    const { options, onElementClick, legend } = this.props;

    let { data } = this.state;
    let dataChart = {
      labels: ["Expired[1]", "Under Process[1]", "Rejected[2]", "Draft[2]"],
      datasets: [
        {
          backgroundColor: ["#d39e00", "#00d32d", "#dc3545", "#0062cc"],
          hoverBackgroundColor: ["#d39e00", "#00d32d", "#dc3545", "#0062cc"],
          data: data
        }
      ]
    };

    return (
      <div>
        <div className="data_req_stat hidden-media">
          {data.length && <Pie width={250} height={50} data={dataChart} />}
        </div>
        {/* <div className="show-media-stat">
          <div style={{ backgroundColor: "#d39e00" }}>Expired 1</div>
          <div style={{ backgroundColor: "#00d32d" }}>Under Process 1</div>
          <div style={{ backgroundColor: "#dc3545" }}>Rejected 2</div>
          <div style={{ backgroundColor: "#0062cc" }}>Draft 2</div>
        </div> */}
      </div>
    );
  }
}

export default Statistics;
