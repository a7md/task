import React, { Component } from "react";
import { Dropdown, Avatar, Icon } from "antd";
import { Link } from "react-router-dom";
export default class Header extends Component {
  render() {
    return (
      <>
        <nav className="navbar navbar-dark bg-dark cust-drop-layout">
          <a className="navbar-brand" href="#">
            Gas Station
          </a>
          <div className="right-drop drop-lay">
            <div className="btn-group dropleft ">
              <button
                className="btn btn-secondary dropdown-toggle dropdown-button cust-drop-btn"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <Icon type="alert" />
              </button>
              <div
                className="dropdown-menu "
                aria-labelledby="dropdownMenuButton"
              >
                <a className="dropdown-item" href="#">
                  Action
                </a>
                <a className="dropdown-item" href="#">
                  Another action
                </a>
                <a className="dropdown-item" href="#">
                  Something else here
                </a>
              </div>
            </div>
            <div btn-group dropdown>
              <button
                className="btn btn-secondary dropdown-toggle dropdown-button cust-drop-btn"
                type="button"
                id="dropdownMenuButton2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <Avatar style={{ backgroundColor: "#87d068" }} icon="user" />
              </button>
              <div
                className="dropdown-menu right-l"
                aria-labelledby="dropdownMenuButton2"
              >
                <Link to="/admin" className="dropdown-item" href="#">
                  Admin
                </Link>
                <a className="dropdown-item" href="#">
                  Logout
                </a>
              </div>
            </div>
          </div>
        </nav>
      </>
    );
  }
}
