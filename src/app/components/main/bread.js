import React, { Component } from "react";
import { Breadcrumb } from "antd";
export default class Bread extends Component {
  render() {
    const { data } = this.props;
    console.log("bre", data);
    return (
      <>
        <Breadcrumb style={{ margin: "16px 0" }}>
          {data.map(d => {
            return <Breadcrumb.Item>{d}</Breadcrumb.Item>;
          })}
        </Breadcrumb>
      </>
    );
  }
}
