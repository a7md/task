import React, { Component } from "react";
import AppRouting from "./routing/route";
import Loading from "./components/loading";

class App extends Component {
  render() {
    return (
      <section>
        {/* <Loading /> */}
        <AppRouting />
      </section>
    );
  }
}

export default App;
