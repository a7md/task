import { render } from "react-dom";
import App from "./app";
import React from "react";
import { LocaleProvider } from "antd";
import $ from "jquery";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";

render(<App />, document.getElementById("app"));
