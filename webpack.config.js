const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: { minimize: true }
          }
        ]
      },

      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, { loader: 'css-loader', options: { url: false, sourceMap: true } },
        { loader: 'sass-loader', options: { sourceMap: true } }],

      },
      {
        test: /\.less$/,
        use: [{
          loader: "style-loader", options: {
            sourceMap: true
          }
        }, {
          loader: "css-loader", options: {
            sourceMap: true
          }
        }, {
          loader: 'less-loader', options: {
            sourceMap: true,
            modules: true
          }
        }],

      },
      {
        test: /\.scss$/,
        use: [{
          loader: "style-loader", options: {
            sourceMap: true
          }
        }, {
          loader: "css-loader", options: {
            sourceMap: true
          }
        }, {
          loader: 'sass-loader', options: {
            sourceMap: true,
            modules: true
          }
        }],

      },



    ]
  },
  devtool: 'source-map',
  stats: "errors-only",
  plugins: [
    new HtmlWebPackPlugin({
      template: "./vendor/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};